// server.js

// set up ======================================================================
// get all the tools we need
var express  = require('express');
var app      = express();
var port     = process.env.PORT || 5000;
var mongoose = require('mongoose');
var passport = require('passport');
var flash    = require('connect-flash');
require('dotenv').config();
var morgan       = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser   = require('body-parser');
var session      = require('express-session');
var nodemailer = require('nodemailer');
var exphbs  = require('express-handlebars');
var configDB = require('./config/database.js');
var index = require('./routes/index');
var authorize = require('./routes/authorize');
var calendar = require('./routes/calendar');

// configuration ===============================================================
mongoose.connect(configDB.url); // connect to our database

require('./config/passport')(passport); // pass passport for configuration

// set up our express application
app.use(express.static(__dirname + '/assets'));
//  create an array to manage our entries
// var entries = [];
// app.locals.entries = entries; // now entries can be accessed in .ejs files
app.use(morgan('dev')); // log every request to the console
app.use(cookieParser()); // read cookies (needed for auth)
app.use(bodyParser.json()); // get information from html forms
app.use(bodyParser.urlencoded({ extended: true }));
app.engine('.hbs', exphbs({extname: '.hbs'}));

app.set('view engine', '.hbs'); // set up ejs for templating
app.use('/index1', index);
app.use('/authorize', authorize);
app.use('/calendar', calendar);

// required for passport
app.use(session({
    secret: 'ilovescotchscotchyscotchscotch', // session secret
    resave: true,
    saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
  
    // render the error page
    res.status(err.status || 500);
    res.render('error');
  });
// routes ======================================================================
require('./app/routes.js')(app, passport); // load our routes and pass in our app and fully configured passport
require('./utils/seeder.js')(app)  // load seed data
// launch ======================================================================
app.listen(port);
console.log('The magic happens on port ' + port);
module.exports = "server";
