const mongoose = require('mongoose')

const messagesSchema = new mongoose.Schema({

  // _id: { type: Number, required: true },
  info: {
    type: String,
    required: true
  }
},{ versionKey: false })
module.exports = mongoose.model('Messages', messagesSchema)
