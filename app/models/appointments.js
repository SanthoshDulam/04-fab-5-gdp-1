const mongoose = require('mongoose')

const AppointmentsSchema = new mongoose.Schema({

  _id: { type: Number, required: true },
  firstname: {
      type: String,
      required: true
  },
  lastname: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  date: {
    type: String,
    required: true
  },
  from: {
    type: String,
    required: true
  },
  to: {
    type:String,
    required: true
  },
  message: {
    type: String,
    required: true
}
})
module.exports = mongoose.model('Appointments', AppointmentsSchema)
