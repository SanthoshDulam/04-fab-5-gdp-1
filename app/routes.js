const LOG = require('../utils/logger.js')
const Model = require('../app/models/officehours.js')
const Modelmessages = require('../app/models/messages.js')
const Modelappoint = require('../app/models/appointments.js')
const find = require('lodash.find')
const remove = require('lodash.remove')
const notfoundstring = 'officehours'
var session     =   require('express-session'); 
const Window = require('window');
var async = require('async');
var crypto = require('crypto');
var User       = require('../app/models/user');
var path = require("path");
var nodemailer = require('nodemailer');
var bcrypt = require('bcryptjs');
const window = new Window();
module.exports = function(app, passport) {

// normal routes ===============================================================

    // show the home page (will also have our login links)
    app.get('/', function(req, res) {
        res.render('login.ejs', { 
            message: req.flash('loginMessage'),
            message1: req.flash('success'),
            message2: req.flash("loginMessage1")
         });
    });

    // PROFILE SECTION =========================
    app.get('/profile', isLoggedIn, function(req, res) {
        res.render('profile.ejs', {
            user : req.user
        });
    });
  
    app.get('/home', isLoggedIn, function(req, res) {
        res.render('home.hbs', {
            user : req.user
        });
    });
   
    app.get('/index1', isLoggedIn, function(req, res) {
        res.render('index1.ejs', {
            user : req.user
        });
    });
    app.get('/create', isLoggedIn, function(req, res) {
        res.render('create.ejs', {
            user : req.user
        });
    });
    app.get('/contact', isLoggedIn, function(req, res) {
            res.render('contact.ejs', {
                user : req.user
            });
    });
    app.get('/details', isLoggedIn, function(req, res) {
        res.render('details.ejs', {
            user : req.user,
            msg: req.msg,
            
            
        });
    });
    app.get('/paper', isLoggedIn, function(req, res) {
        res.render('paper.ejs', {
            user : req.user
        });
    });
    
    app.get('/forgot', function (req, res) {
        res.render('forgot.ejs', {
             message: req.flash('error_msg'),
             message1: req.flash('success_msg'),
     });
      });
    app.get('/facultydetails', isLoggedIn, function(req, res) {
        res.render('facultydetails.ejs', {
            user : req.user
        });
    });
    app.get('/appointment', isLoggedIn, function(req, res) {
        res.render('appointment.ejs', {
            user : req.user
        });
    });
    app.get('/addmessage', isLoggedIn, function(req, res) {
        res.render('addmessage.ejs', {
            user : req.user
        });
    });
    app.get('/edit', isLoggedIn, function(req, res) {
        res.render('edit.ejs', {
            user : req.user
        })
    });
    app.get('/delete', isLoggedIn, function(req, res) {
        res.render('delete.ejs', {
            user : req.user
        });
    });
    app.get('/reject', isLoggedIn, function(req, res) {
        res.render('reject.ejs', {
            user : req.user
        });
    });
    app.get('/changeview', isLoggedIn, function(req, res) {
        res.render('changeview.ejs', {
            user : req.user
        });
    });
    // LOGOUT ==============================
    app.get('/logout', function(req, res) {
        req.logout();
        res.redirect('/');
    });
    app.get('/delete/:id', (req, res) => {
        LOG.info(`Handling GET /details/:id ${req}`)
        const id = parseInt(req.params.id, 10) // base 10
        const data = req.app.locals.officehours.query
        const item = find(data, { _id: id })
        if (!item) { return res.end(notfoundstring) }
        LOG.info(`RETURNING VIEW FOR ${JSON.stringify(item)}`)
        res.render('delete.ejs',
          {
            officehour : item,
            user : req.user
          })
      })
      app.get('/reject/:id', (req, res) => {
        LOG.info(`Handling GET /details/:id ${req}`)
        const id = parseInt(req.params.id, 10) // base 10
        const data = req.app.locals.appointments.query
        const item = find(data, { _id: id })
        if (!item) { return res.end(notfoundstring) }
        LOG.info(`RETURNING VIEW FOR ${JSON.stringify(item)}`)
        res.render('reject.ejs',
          {
            appointment : item,
            user : req.user
          })
      })
      app.get('/edit/:id', (req, res) => {
        LOG.info(`Handling GET /details/:id ${req}`)
        const id = parseInt(req.params.id, 10) // base 10
        const data = req.app.locals.officehours.query
        const item = find(data, { _id: id })
        if (!item) { return res.end(notfoundstring) }
        LOG.info(`RETURNING VIEW FOR ${JSON.stringify(item)}`)
        res.render('edit.ejs',
          {
            officehour : item,
            user : req.user
          })
      })

// =============================================================================
// AUTHENTICATE (FIRST LOGIN) ==================================================
// =============================================================================

    // locally --------------------------------
        // LOGIN ===============================
        // show the login form
        app.get('/login', function(req, res) {
            res.render('login.ejs', { 
                message: req.flash('loginMessage'),
                message1: req.flash('success'),
                message2: req.flash("loginMessage1")
             });
        });
        app.get('/login1', function(req, res) {
            res.render('login1.ejs', { message: req.flash('loginMessage') });
        });

        // process the login form
        app.post('/login', passport.authenticate('local-login', {
            successRedirect : '/index1', // redirect to the secure profile section
            failureRedirect : '/login', // redirect back to the signup page if there is an error
            failureFlash : true // allow flash messages
            
        }));
        app.post('/login1', passport.authenticate('local-login', {
            successRedirect : '/calendar', // redirect to the secure profile section
            failureRedirect : '/login', // redirect back to the signup page if there is an error
            failureFlash : true // allow flash messages
        }));


        // SIGNUP =================================
        // show the signup form
        app.get('/signup', function(req, res) {
            res.render('signup.ejs', { message: req.flash('signupMessage') });
        });
       

        // process the signup form
        app.post('/signup', passport.authenticate('local-signup', {
            successRedirect : '/login', // redirect to the secure profile section
            failureRedirect : '/signup', // redirect back to the signup page if there is an error
            failureFlash : true // allow flash messages
        }));
        app.post('/save', (req, res,done) => {
            LOG.info(`Handling POST ${req}`)
            LOG.debug(JSON.stringify(req.body))
            const data = req.app.locals.officehours.query
            const item = new Model()
            LOG.info(`NEW ID ${req.body._id}`)
            item._id = parseInt(data.length+1, 10) // base 10
            item.day = req.body.day
            item.starttime = req.body.starttime
            item.endtime = req.body.endtime 
            // item.save(function(err) {
            //     if (err)
            //         return done(err);

            //     return done(null, item);
            // });
                data.push(item)
              LOG.info(`SAVING NEW officehours ${JSON.stringify(item)}`)
              return res.redirect('/facultydetails')
            // }
          })
          app.post('/savemessage', (req, res,done) => {
            LOG.info(`Handling POST ${req}`)
            LOG.debug(JSON.stringify(req.body))
            const data = req.app.locals.messages.query
            const item = new Modelmessages(req.body)
            // LOG.info(`NEW ID ${req.body._id}`)
            // item._id = parseInt(req.body._id, 10) // base 10
            item.messageinfo = req.body.messageinfo
            // item.save(function(err) {
            //     if (err)
            //         return done(err);

            //     return done(null, item);
            // });
                data.push(item)
              LOG.info(`SAVING NEW officehours ${JSON.stringify(item)}`)
              return res.redirect('/addmessage')
            // }
          })
          app.post('/save/:id', (req, res) => {
            LOG.info(`Handling SAVE request ${req}`)
            const id = parseInt(req.params.id, 10) // base 10
            LOG.info(`Handling SAVING ID=${id}`)
            const data = req.app.locals.officehours.query
            const item = find(data, { _id: id })
            if (!item) { return res.end(notfoundstring) }
            LOG.info(`ORIGINAL VALUES ${JSON.stringify(item)}`)
            LOG.info(`UPDATED VALUES: ${JSON.stringify(req.body)}`)
            item.day = req.body.day
            item.starttime = req.body.starttime
            item.endtime = req.body.endtime 
              LOG.info(`SAVING UPDATED hours ${JSON.stringify(item)}`)
              return res.redirect('/facultydetails')
            // }
          })
          // DELETE id (uses HTML5 form method POST)
    app.post('/delete/:id', (req, res) => {
    LOG.info(`Handling DELETE request ${req}`)
    const id = parseInt(req.params.id, 10) // base 10
    LOG.info(`Handling REMOVING ID=${id}`)
    const data = req.app.locals.officehours.query
    const item = find(data, { _id: id })
    if (!item) {
      return res.end(notfoundstring)
    }
    if (item.isActive) {
      item.isActive = false
      console.log(`Deactivated item ${JSON.stringify(item)}`)
    } else {
      const item = remove(data, { _id: id })
      console.log(`Permanently deleted item ${JSON.stringify(item)}`)
    }
    return res.redirect('/facultydetails')
  })
  app.post('/reject/:id', (req, res) => {
    const id = parseInt(req.params.id, 10) // base 10
    const data1 = req.app.locals.appointments.query
    const item = find(data1, { _id: id })
    console.log("checking mail")
    var api_key = 'key-332b09f392b741b368c580bdd74734d7';
    var domain = 'sandbox3ba9ea10b2b443e3ae2dee053627db44.mailgun.org';
    var mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});
    var data = {
    from: 'Mail Gun <postmaster@sandbox3ba9ea10b2b443e3ae2dee053627db44.mailgun.org>', 
   to: 'S530666@nwmissouri.edu',
   subject: "Appointment rejected ",
   text: "Hi "+item.lastname+",\n"+
   "Your Appointment on "+item.date+ " from "+item.from+ " to "
   +item.to+" is rejected by "+req.user.local.name};
   mailgun.messages().send(data, function (error, body) {
   console.log(body);
   console.log(error);
   if(!error)
       {
        LOG.info(`Handling DELETE request ${req}`)
     
        LOG.info(`Handling REMOVING ID=${id}`)
        if (!item) {
          return res.end(notfoundstring)
        }
        if (item.isActive) {
          item.isActive = false
          console.log(`Deactivated item ${JSON.stringify(item)}`)
        } else {
          const item = remove(data1, { _id: id })
          console.log(`Permanently deleted item ${JSON.stringify(item)}`)
        }
        return res.redirect('/appointment')
           // res.render('appointment.ejs', { message: req.flash('loginMessage') });
          
       }
   else
   response.send("mail not sent");
   
   
   
   });
    
  })
  app.post('/accept/:id', (req, res) => {
    const id = parseInt(req.params.id, 10) // base 10
    const data1 = req.app.locals.appointments.query
    const item = find(data1, { _id: id })
    console.log("checking mail")


   var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: 'gdp2.fastrack@gmail.com',
      pass: 'gdp21234'
    }
  });

  let content = 'BEGIN:VCALENDAR\r\nPRODID:-//ACME/DesktopCalendar//EN\r\nMETHOD:REQUEST\r\nBEGIN:VEVENT\r\nDTSTART:20181215\r\nDTEND:20181216\r\nDTSTAMP:20150421T141403\r\nDESCRIPTION:Office Hours\r\nEND:VEVENT\r\nEND:VCALENDAR';

  var mailOptions = {
    from: 'gdp2.fastrack@gmail.com',
    to: 'dscpsanthosh.518@gmail.com',
    subject: 'Accepted Meeting Invitation',
    html: "Hi "+item.lastname+",\n"+
    "Your Appointment on "+item.date+ " from "+item.from+ " to "
    +item.to+" is accepted by "+req.user.local.name,
    icalEvent: {
        filename: 'invitation.ics',
        method: 'request',
        content: content
    }
  };
//   console.log(request.body.email1);
  transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
      console.log(error);
    } else {
      console.log('Email sent: ' + info.response);
      LOG.info(`Handling DELETE request ${req}`)
     
        LOG.info(`Handling REMOVING ID=${id}`)
        if (!item) {
          return res.end(notfoundstring)
        }
        if (item.isActive) {
          item.isActive = false
          console.log(`Deactivated item ${JSON.stringify(item)}`)
        } else {
          const item = remove(data1, { _id: id })
          console.log(`Permanently deleted item ${JSON.stringify(item)}`)
        }
        return res.redirect('/appointment')
    }
  });

  })
  app.post('/facultydetails',function(req,res) {
    // const msg= req.body.messages;
    // console.log(msg);
     res.render('details.ejs',{
        msg:req.body.messages1,
        user:req.user

    })
    });




    // locally --------------------------------
        app.get('/connect/local', function(req, res) {
            res.render('connect-local.ejs', { message: req.flash('loginMessage') });
        });
        app.post('/connect/local', passport.authenticate('local-signup', {
            successRedirect : '/profile', // redirect to the secure profile section
            failureRedirect : '/connect/local', // redirect back to the signup page if there is an error
            failureFlash : true // allow flash messages
        }));
        app.post("/contact", function(request,response){
            console.log("checking mail")
            console.log("body"+request.body)
             var api_key = 'key-332b09f392b741b368c580bdd74734d7';
             var domain = 'sandbox3ba9ea10b2b443e3ae2dee053627db44.mailgun.org';
             var mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});
             var data = {
             from: 'Mail Gun <postmaster@sandbox3ba9ea10b2b443e3ae2dee053627db44.mailgun.org>', 
            to: 'dscpsanthosh.518@gmail.com',
            subject: "Meeting Request by "+ request.body.firstname,
            text: "Full Name: "+request.body.firstname+", "+request.body.lastname+
            "\n\nEmail: "+request.body.email+"\n\n"+request.body.message+" on "
            +request.body.date+ " from "+request.body.from+ " to "
            +request.body.to};
            mailgun.messages().send(data, function (error, body) {
            console.log(body);
            console.log(error);
            if(!error)
                {
                    LOG.info(`Handling POST ${request}`)
                    LOG.debug(JSON.stringify(request.body))
                    const data1 = request.app.locals.appointments.query
                    const item = new Modelappoint()
                    LOG.info(`NEW ID ${request.body._id}`)
                    item._id = parseInt(data1.length+1, 10)
                    item.firstname = request.body.firstname
                    item.lastname = request.body.lastname
                    item.email = request.body.email
                    item.date = request.body.date
                    item.from = request.body.from
                    item.to = request.body.to 
                    item.message = request.body.message
                    // item.save(function(err) {
                    //     if (err)
                    //         return done(err);
        
                    //     return done(null, item);
                    // });
                        data1.push(item)
                      LOG.info(`SAVING NEW appointment ${JSON.stringify(item)}`)
                      return response.redirect('/details')
                    // response.render('contact.ejs', { message: request.flash('loginMessage') });
                   
                }
            else
            response.send("mail not sent");        
            });
            });

            app.post("/paper", function(request,response){
                console.log("checking mail")
                 var api_key = 'key-332b09f392b741b368c580bdd74734d7';
                 var domain = 'sandbox3ba9ea10b2b443e3ae2dee053627db44.mailgun.org';
                 var mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});
                //  var filepath = path.join(__dirname, request.body.image);
                 var data = {
                 from: 'Mail Gun <postmaster@sandbox3ba9ea10b2b443e3ae2dee053627db44.mailgun.org>', 
                to: 'dscpsanthosh.518@gmail.com',
                subject: "Paper DropOff by "+ request.body.firstname,
                text: "Full Name: "+request.body.firstname+", "+request.body.lastname+
                "\n\nEmail: "+request.body.email+"\n\n"+request.body.message+" Image :"
                +request.body.img};
                mailgun.messages().send(data, function (error, body) {
                console.log(body);
                console.log(error);
                if(!error)
                    {
                        response.render('paper.ejs', { message: request.flash('loginMessage') });
                       
                    }
                else
                response.send("mail not sent");
                
                
                
                });
                });
  //Forgot

  app.post('/forgote', function(req, res, next) {
      console.log("hi")
    async.waterfall([
      function(done) {
        crypto.randomBytes(20, function(err, buf) {
          var token = buf.toString('hex');
          done(err, token);
        });
      },
      function(token, done) {
        User.findOne({'local.email': req.body.email }, function(err, user) {
         console.log(user)
          if (!user) {
            req.flash('error_msg', 'No account with that email address exists.');
            return res.redirect('back');
          }
         
          user.local.resetPasswordToken = token;
          user.local.resetPasswordExpires = Date.now() + 3600000; // 1 hour
  
          user.save(function(err) {
            done(err, token, user);
          });
        });
      },
      function(token, user, done) {
        var smtpTransport = nodemailer.createTransport({
          service: 'gmail',
          auth: {
            user: 'gdp2.fastrack@gmail.com',
            pass: 'gdp21234'
          }
        });
        var mailOptions = {
          to: req.body.email,
          from: 'passwordreset@demo.com',
          subject: 'Node.js Password Reset',
          text: 'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n' +
            'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
            'http://' + req.headers.host + '/reset/' + token + '\n\n' +
            'If you did not request this, please ignore this email and your password will remain unchanged.\n'
        };
        smtpTransport.sendMail(mailOptions, function(err) {
          req.flash('success_msg', 'An e-mail has been sent to ' + req.body.email + ' with further instructions.');
          done(err, 'done');
        });
      }
    ], function(err) {
      if (err) return next(err);
      res.redirect('/forgot');
    });
  }); 
  
  
        
  app.get('/reset/:token', function(req, res) {
    User.findOne({'local.resetPasswordToken': req.params.token,'local.resetPasswordExpires': { $gt: Date.now() } }, function(err, user) {
      if (!user) {
        req.flash('error', 'Password reset token is invalid or has expired.');
        return res.redirect('/login');
      }
      res.render('forgotP.ejs', {
        token: req.params.token
      });
    });
  });
  
  //Reset
  app.post('/reset/:token', function(req, res) {
    async.waterfall([
      function(done) {
        User.findOne({ 'local.resetPasswordToken': req.body.token, 'local.resetPasswordExpires': { $gt: Date.now() } }, function(err, user) {
          if (!user) {
            req.flash('error', 'Password reset token is invalid or has expired.');
            return res.redirect('/login');
          }
  
   
              user.local.password = user.generateHash(req.body.password);
              user.local.resetPasswordToken = undefined;
              user.local.resetPasswordExpires = undefined;
              user.save(function(err) {
                req.logIn(user, function(err) {
                   done(err, user);
                });
               }); 
        });
      },
      function(user, done) {
        var smtpTransport = nodemailer.createTransport({
          service: 'SendGrid',
          auth: {
            user: 'gdp2.fastrack@gmail.com',
            pass: 'gdp21234'
          }
        });
        var mailOptions = {
          to: req.user.email,
          from: 'passwordreset@demo.com',
          subject: 'Your password has been changed',
          text: 'Hello,\n\n' +
            'This is a confirmation that the password for your account ' + req.user.email + ' has just been changed.\n'
        };
        smtpTransport.sendMail(mailOptions, function(err) {
          req.flash('success', 'Success! Your password has been changed.');
          done(err);
        });
      }
    ], function(err) {
      res.redirect('/login');
    });
  });


    


};

// route middleware to ensure user is logged in
function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();

    res.redirect('/');
}
